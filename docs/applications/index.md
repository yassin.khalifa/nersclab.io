# Applications

NERSC provides a wide variety of pre-built applications, optimized for
our systems. The primary way these are provided in the [NERSC
environment](../environment/index.md) is through
modules.

!!! tip "Best practices"
    [Best practices](../jobs/best-practices.md) are not specific to any
    one application and ensure efficient use your project's allocation.

!!! note
    Some available applications may be present, but may not
    necessarily have a dedicated page on this site. Please consult the
    `module avail` command on a NERSC system to see what is available.

In addition to application specific details NERSC also provides:

* additional [example jobs](../jobs/examples/index.md)
* [workflow tools](../jobs/workflow-tools.md) for coordination/ automation.
* A [guide](../getting-started.md) for new users.

## Popular applications

The tables here summarize the latest version available at NERSC, for
different architectures, of several popular applications. **Note** that
the Perlmutter software stack is still being built out: some applications
are available at this time. For Perlmutter, these tables indicate
applications that are available (as of 06/01/2023).

### Density functional theory

| Application                                   | Perlmutter GPU  | Perlmutter CPU  |
|-----------------------------------------------|-----------------|-----------------|
| [BerkeleyGW](berkeleygw/index.md)             | 3.x             | 3.x             |
| [CP2K](cp2k/index.md)                         | 2022.1 (docker) | 2022.1 (docker) |
| [SIESTA](siesta/index.md)                     | -               | 4.0.2 (spack)   |
| [Quantum ESPRESSO](quantum-espresso/index.md) | 7.x             | 7.x             |
| [VASP](vasp/index.md)                         | 6.x             | 5.4, 6.x        |
| [Wannier90](wannier90/index.md)               | 3.1.0           | -               |

### Molecular dynamics

| Application                                   | Perlmutter GPU  | Perlmutter CPU  |
|-----------------------------------------------|-----------------|-----------------|
| [AMBER](amber/index.md)                       | 20              | 20              |
| [Abinit](abinit/index.md)                     | -               | -               |
| [Gromacs](gromacs/index.md)                   | 2022.3          | 2021.5-plumed   |
| [LAMMPS](lammps/index.md)                     | 2022.11.03      | 2022.11.03      |
| [NAMD](namd/index.md)                         | 2.15a2          | 2.15a2          |

### Chemistry applications

| Application                                   | Perlmutter GPU  | Perlmutter CPU  |
|-----------------------------------------------|-----------------|-----------------|
| [GAMESS](gamess/index.md)                     | 9/30/2022 R2    | 9/30/2022 R2    |
| [MOLPRO](molpro/index.md)                     | -               | -               |
| [Q-Chem](qchem/index.md)                      | -               | -               |
| [NWChem](nwchem/index.md)                     | (future)        | (future)        |

### Mathematical environments

| Application                                   | Perlmutter GPU  | Perlmutter CPU  |
|-----------------------------------------------|-----------------|-----------------|
| [Mathematica](mathematica/index.md)           | 13.0.1          | 13.0.1          |
| [MATLAB](matlab/index.md)                     | R2021b          | R2021b          |

### Visualization

| Application                                   | Perlmutter GPU  | Perlmutter CPU  |
|-----------------------------------------------|-----------------|-----------------|
| [ParaView](paraview/index.md)                 | 5.11.1          | 5.11.1          |

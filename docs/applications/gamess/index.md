# GAMESS

The General Atomic and Molecular Electronic Structure System (GAMESS)
is a general ab initio quantum chemistry package. Please see 
the [GAMESS home page](https://www.msg.chem.iastate.edu/gamess/)
for more information.

## Availability and Supported Architectures at NERSC

GAMESS runs on GPUs and can be run via a 
[container](https://catalog.ngc.nvidia.com/orgs/hpc/containers/gamess). Users
are welcome to build and run GAMESS on NERSC.

## Application Information, Documentation, and Support

[GAMESS official documentation](https://www.msg.chem.iastate.edu/gamess/documentation.html)

## Related Applications

* [MOLPRO](../molpro/index.md)
* [Q-chem](../qchem/index.md)
* [NWChem](../nwchem/index.md)

## User Contributed Information

!!! info "Please help us improve this page"
	Users are invited to contribute helpful information and corrections
	through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
